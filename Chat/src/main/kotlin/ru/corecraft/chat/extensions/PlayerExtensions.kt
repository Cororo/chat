package ru.corecraft.chat.extensions

import org.bukkit.entity.Player
import ru.corecraft.chat.ChatManager

var Player.prefix: String?
    get() = ChatManager.chat.getPlayerPrefix(this)
    set(prefix) = ChatManager.chat.setPlayerPrefix(this, prefix)

var Player.suffix: String?
    get() = ChatManager.chat.getPlayerSuffix(this)
    set(prefix) = ChatManager.chat.setPlayerSuffix(this, prefix)
