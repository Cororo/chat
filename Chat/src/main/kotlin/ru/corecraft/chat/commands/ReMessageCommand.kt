package ru.corecraft.chat.commands

import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import ru.corecraft.chat.ChatManager

class ReMessageCommand: CommandExecutor {
    private val usage = "/r <сообщение>"

    override fun onCommand(
        sender: CommandSender,
        command: Command,
        label: String,
        args: Array<String>
    ): Boolean {
        if (args.isEmpty()) {
            sender.sendMessage("§cНеверное использование!")
            sender.sendMessage("§cИспользование: $usage")
            return false
        }
        val send: Player? = ChatManager.messageList[sender as Player]
        if (send == null) {
            sender.sendMessage("§cВам никто не отправлял сообщений!")
            sender.sendMessage("§cИспользование: $usage")
            return false
        }
        val message = args.joinToString(" ").replace("&", "§")
        sender.sendMessage("§8[§cЯ §8-> §c ${send.displayName} §8] §f$message")
        send.sendMessage("§8[§c${sender.displayName} §8-> §cЯ§8] §f$message")
        return true
    }

}